package com.mcm.translator;

import com.mcm.translator.commands.CommandLanguage;

public class RegisterCommands {

    public static void register() {
        Main.instance.getCommand("language").setExecutor(new CommandLanguage());
        Main.instance.getCommand("idioma").setExecutor(new CommandLanguage());
        Main.instance.getCommand("sprache").setExecutor(new CommandLanguage());
        Main.instance.getCommand("lingua").setExecutor(new CommandLanguage());
        Main.instance.getCommand("язык").setExecutor(new CommandLanguage());
        Main.instance.getCommand("语言").setExecutor(new CommandLanguage());
        Main.instance.getCommand("언어").setExecutor(new CommandLanguage());
        Main.instance.getCommand("taal").setExecutor(new CommandLanguage());
        Main.instance.getCommand("Język").setExecutor(new CommandLanguage());
        Main.instance.getCommand("言語").setExecutor(new CommandLanguage());
    }
}
