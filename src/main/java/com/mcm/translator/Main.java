package com.mcm.translator;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static String ip = "localhost";
    public static String table = "mcmedieval_languages";
    public static String user = "admin";
    public static String password = "r4TzoVQU";

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        MySql.connect();
        RegisterMain.register();
        RegisterListeners.register();
        RegisterCommands.register();
    }

    @Override
    public void onDisable() {
        MySql.disconnect();
    }
}
