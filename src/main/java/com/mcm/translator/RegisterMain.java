package com.mcm.translator;

import com.mcm.translator.database.TradutionDb;
import com.mcm.translator.survival_messages.MessagesBuilder;
import org.bukkit.Bukkit;

public class RegisterMain {

    public static void register() {
        TradutionDb.setAllTradutionsInCache();

        Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                MessagesBuilder.apply();
            }
        }, 20L);
    }
}
