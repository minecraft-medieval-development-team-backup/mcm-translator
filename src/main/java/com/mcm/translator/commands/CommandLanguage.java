package com.mcm.translator.commands;

import com.mcm.translator.managers.InventoryLanguages;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandLanguage implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;

        if (command.getName().equalsIgnoreCase("language") || command.getName().equalsIgnoreCase("idioma") || command.getName().equalsIgnoreCase("sprache") || command.getName().equalsIgnoreCase("lingua")
                || command.getName().equalsIgnoreCase("язык") || command.getName().equalsIgnoreCase("lalangue") || command.getName().equalsIgnoreCase("语言") || command.getName().equalsIgnoreCase("언어")
                || command.getName().equalsIgnoreCase("taal") || command.getName().equalsIgnoreCase("Język") || command.getName().equalsIgnoreCase("言語")) {
            player.openInventory(InventoryLanguages.getInventory());
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0f, 1.0f);
        }
        return false;
    }
}
