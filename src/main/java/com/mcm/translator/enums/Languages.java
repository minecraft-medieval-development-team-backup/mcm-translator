package com.mcm.translator.enums;

public enum Languages {
    english, portuguese, spanish, german, italian, russian, french, chinese, korean, dutch, polish, japanese;
}