package com.mcm.translator.listeners;

import com.mcm.translator.database.LanguageDb;
import com.mcm.translator.database.TradutionDb;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryLanguages implements Listener {

    @EventHandler
    public void onInteract(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getView().getTitle().equalsIgnoreCase("Languages: ")) {
            if (event.getSlot() == 10) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "english");
                } else LanguageDb.updateLanguage(uuid, "english");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "english"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 11) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "portuguese");
                } else LanguageDb.updateLanguage(uuid, "portuguese");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "portuguese"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 12) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "spanish");
                } else LanguageDb.updateLanguage(uuid, "spanish");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "spanish"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 13) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "german");
                } else LanguageDb.updateLanguage(uuid, "german");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "german"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 14) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "italian");
                } else LanguageDb.updateLanguage(uuid, "italian");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "italian"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 15) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "russian");
                } else LanguageDb.updateLanguage(uuid, "russian");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "russian"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 16) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "french");
                } else LanguageDb.updateLanguage(uuid, "french");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "french"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 20) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "chinese");
                } else LanguageDb.updateLanguage(uuid, "chinese");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "chinese"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 21) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "korean");
                } else LanguageDb.updateLanguage(uuid, "korean");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "korean"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 22) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "dutch");
                } else LanguageDb.updateLanguage(uuid, "dutch");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "dutch"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 23) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "polish");
                } else LanguageDb.updateLanguage(uuid, "polish");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "polish"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            } else if (event.getSlot() == 24) {
                if (LanguageDb.getLanguage(uuid) == null) {
                    LanguageDb.setLanguage(uuid, "japanese");
                } else LanguageDb.updateLanguage(uuid, "japanese");
                player.closeInventory();
                player.sendMessage(TradutionDb.getMessage("4VJbvy#uSv&R4Kk", "japanese"));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
            }
        }
    }
}
