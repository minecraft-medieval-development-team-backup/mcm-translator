package com.mcm.translator.listeners;

import com.mcm.translator.Main;
import com.mcm.translator.database.LanguageDb;
import com.mcm.translator.managers.InventoryLanguages;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinEvents implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (LanguageDb.getLanguage(uuid) == null) {
            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    player.openInventory(InventoryLanguages.getInventory());
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0f, 1.0f);
                }
            }, 20L);
        }
    }
}
