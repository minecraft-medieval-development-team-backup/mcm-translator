package com.mcm.translator.database;

import java.util.HashMap;

public class TradutionCache {

    private String id;
    private String tradution;
    private static HashMap<String, TradutionCache> cache = new HashMap<String, TradutionCache>();

    public TradutionCache(String id, String tradution) {
        this.id = id;
        this.tradution = tradution;
    }

    public TradutionCache insert() {
        this.cache.put(this.id, this);
        return this;
    }

    public static TradutionCache get(String id) {
        return cache.get(id);
    }

    public String getTradution() {
        return this.tradution;
    }

    public void setTradution(String tradution) {
        this.tradution = tradution;
    }
}

