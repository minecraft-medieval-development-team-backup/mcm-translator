package com.mcm.translator.database;

import com.mcm.translator.Main;
import com.mcm.translator.MySql;
import org.bukkit.Bukkit;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LanguageDb {

    public static String getLanguage(String uuid) {
        if (LanguageCache.get(uuid) != null) {
            return LanguageCache.get(uuid).getLanguage();
        } else return getLanguageDb(uuid);
    }

    public static void updateLanguage(final String uuid, final String language) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("UPDATE language_data SET language = ? WHERE uuid = ?");
                    statement.setString(2, uuid);
                    statement.setString(1, language);
                    statement.executeUpdate();
                    if (LanguageCache.get(uuid) == null) new LanguageCache(uuid, language).insert(); else LanguageCache.get(uuid).setLanguage(language);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void setLanguage(String uuid, String language) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO language_data(uuid, language) VALUES (?, ?)");
            statement.setString(1, uuid);
            statement.setString(2, language);
            statement.executeUpdate();
            new LanguageCache(uuid, language).insert();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static String getLanguageDb(String uuid) {
        try {
            PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM language_data WHERE uuid = ?");
            statement.setString(1, uuid);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getString("language");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
