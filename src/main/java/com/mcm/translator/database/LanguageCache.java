package com.mcm.translator.database;

import java.util.HashMap;

public class LanguageCache {

    private String uuid;
    private String language;
    private static HashMap<String, LanguageCache> cache = new HashMap<String, LanguageCache>();

    public LanguageCache(String uuid, String language) {
        this.uuid = uuid;
        this.language = language;
    }

    public LanguageCache insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static LanguageCache get(String uuid) {
        return cache.get(uuid);
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
