package com.mcm.translator.database;

import com.mcm.translator.enums.Languages;
import com.mcm.translator.Main;
import com.mcm.translator.MySql;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TradutionDb {

    public static List<String> ids = new ArrayList<String>();

    public static List<String> english = new ArrayList<String>();
    public static List<String> portuguese = new ArrayList<String>();
    public static List<String> spanish = new ArrayList<String>();
    public static List<String> german = new ArrayList<String>();
    public static List<String> italian = new ArrayList<String>();
    public static List<String> russian = new ArrayList<String>();
    public static List<String> french = new ArrayList<String>();
    public static List<String> chinese = new ArrayList<String>();
    public static List<String> korean = new ArrayList<String>();
    public static List<String> dutch = new ArrayList<String>();
    public static List<String> polish = new ArrayList<String>();
    public static List<String> japanese = new ArrayList<String>();


    public static String getMessage(String id, String uuid) {
        if (TradutionCache.get(id + LanguageDb.getLanguage(uuid)) != null) {
            return TradutionCache.get(id + LanguageDb.getLanguage(uuid)).getTradution();
        } else if (TradutionCache.get(id + "english") != null) {
            return TradutionCache.get(id + "english").getTradution();
        } else if (TradutionCache.get(id + "portuguese") != null) {
            return TradutionCache.get(id + "portuguese").getTradution();
        } else return ChatColor.RED + "THIS MESSAGE ID NOT EXIST, ID: " + id + "!";
    }

    public static void setAllTradutionsInCache() {
        Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = MySql.connection.prepareStatement("SELECT * FROM tradutions");
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        String id = rs.getString("id");
                        try {
                            PreparedStatement st = MySql.connection.prepareStatement("SELECT * FROM tradutions WHERE id = ?");
                            st.setString(1, id);
                            ResultSet rs2 = st.executeQuery();
                            while (rs2.next()) {
                                String[] languages = {"english", "portuguese", "spanish", "german", "italian", "russian", "french", "chinese", "korean", "dutch", "polish", "japanese"};
                                for (String language : languages) {
                                    if (!ids.contains(id)) ids.add(id);
                                    String result = rs2.getString(language);
                                    if (result != null) {
                                        new TradutionCache(id + language, result).insert();
                                        if (language.equals("english")) english.add(id);
                                        else if (language.equals("portuguese")) portuguese.add(id);
                                        else if (language.equals("spanish")) spanish.add(id);
                                        else if (language.equals("german")) german.add(id);
                                        else if (language.equals("italian")) italian.add(id);
                                        else if (language.equals("russian")) russian.add(id);
                                        else if (language.equals("french")) french.add(id);
                                        else if (language.equals("chinese")) chinese.add(id);
                                        else if (language.equals("korean")) korean.add(id);
                                        else if (language.equals("dutch")) dutch.add(id);
                                        else if (language.equals("polish")) polish.add(id);
                                        else if (language.equals("japanese")) japanese.add(id);
                                    }
                                }
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void createNewMessage(final String id, final String message) {
        if (!ids.contains(id)) {
            Bukkit.getScheduler().runTaskAsynchronously(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    try {
                        PreparedStatement statement = MySql.connection.prepareStatement("INSERT INTO tradutions(id, english, portuguese, spanish, german, italian, russian, french, chinese, korean, dutch, polish, japanese) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                        statement.setString(1, id);
                        statement.setString(2, null);
                        statement.setString(3, message);
                        statement.setString(4, null);
                        statement.setString(5, null);
                        statement.setString(6, null);
                        statement.setString(7, null);
                        statement.setString(8, null);
                        statement.setString(9, null);
                        statement.setString(10, null);
                        statement.setString(11, null);
                        statement.setString(12, null);
                        statement.setString(13, null);
                        statement.executeUpdate();
                        ids.add(id);
                        portuguese.add(id);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }
}
