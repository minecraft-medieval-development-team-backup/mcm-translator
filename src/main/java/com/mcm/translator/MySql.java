package com.mcm.translator;

import org.bukkit.ChatColor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySql {

    public static Connection connection;

    public static void connect() {
        if (!isConnected()) {
            try {
                connection = DriverManager.getConnection("jdbc:mysql://" + Main.ip + "/" + Main.table + "?autoReconnect=true&useSSL=false", Main.user,
                        Main.password);
                System.out.println(ChatColor.GREEN + "MYSQL CONECTADO 'TRANSLATOR'!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void disconnect() {
        if (isConnected()) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean isConnected() {
        return (connection != null);
    }
}
