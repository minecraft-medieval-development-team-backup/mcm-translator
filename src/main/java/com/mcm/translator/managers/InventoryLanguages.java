package com.mcm.translator.managers;

import com.mcm.translator.database.TradutionDb;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagList;
import net.minecraft.server.v1_15_R1.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import java.util.ArrayList;

public class InventoryLanguages {

    public static Inventory getInventory() throws ArithmeticException {
        Inventory inventory = Bukkit.createInventory(null, 4 * 9, "Languages: ");

        ItemStack japanese = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "6a140d7c-ecaf-48ec-879b-3da8d7f1d316", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZDY0MGFlNDY2MTYyYTQ3ZDNlZTMzYzQwNzZkZjFjYWI5NmYxMTg2MGYwN2VkYjFmMDgzMmM1MjVhOWUzMzMyMyJ9fX0=");
        SkullMeta metajapanese = (SkullMeta) japanese.getItemMeta();
        metajapanese.setDisplayName(ChatColor.GREEN + "日本語");
        ArrayList<String> lorejapanese = new ArrayList<String>();
        lorejapanese.add(ChatColor.GRAY + "言語を日本語に変更します。");
        lorejapanese.add(" ");
        lorejapanese.add(ChatColor.GRAY + "翻訳率");
        lorejapanese.add(percentMsg((double) TradutionDb.japanese.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.japanese.size() - TradutionDb.ids.size() == 0) {
            lorejapanese.add(ChatColor.GRAY + "完全に翻訳されました！");
        }
        lorejapanese.add(" ");
        lorejapanese.add(ChatColor.YELLOW + "クリックして言語を変更します");
        metajapanese.setLore(lorejapanese);
        japanese.setItemMeta(metajapanese);

        ItemStack polish = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "b635a0bd-3c73-443b-a9e3-372e731ec4a2", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOTIxYjJhZjhkMjMyMjI4MmZjZTRhMWFhNGYyNTdhNTJiNjhlMjdlYjMzNGY0YTE4MWZkOTc2YmFlNmQ4ZWIifX19");
        SkullMeta metapolish = (SkullMeta) polish.getItemMeta();
        metapolish.setDisplayName(ChatColor.GREEN + "Polski");
        ArrayList<String> lorepolish = new ArrayList<String>();
        lorepolish.add(ChatColor.GRAY + "Zmień język na polski.");
        lorepolish.add(" ");
        lorepolish.add(ChatColor.GRAY + "Przetłumaczone procentowo");
        lorepolish.add(percentMsg((double) TradutionDb.polish.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.polish.size() - TradutionDb.ids.size() == 0) {
            lorepolish.add(ChatColor.GRAY + "W pełni przetłumaczone!");
        }
        lorepolish.add(" ");
        lorepolish.add(ChatColor.YELLOW + "Kliknij, aby zmienić język");
        metapolish.setLore(lorepolish);
        polish.setItemMeta(metapolish);

        ItemStack dutch = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "5ddfbff0-7173-48ec-82e6-73343e7fce0f", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYzIzY2YyMTBlZGVhMzk2ZjJmNWRmYmNlZDY5ODQ4NDM0ZjkzNDA0ZWVmZWFiZjU0YjIzYzA3M2IwOTBhZGYifX19");
        SkullMeta metadutch = (SkullMeta) dutch.getItemMeta();
        metadutch.setDisplayName(ChatColor.GREEN + "Nederlands");
        ArrayList<String> loredutch = new ArrayList<String>();
        loredutch.add(ChatColor.GRAY + "Verander je taal in het Nederlands.");
        loredutch.add(" ");
        loredutch.add(ChatColor.GRAY + "Percentage vertaald");
        loredutch.add(percentMsg((double) TradutionDb.dutch.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.dutch.size() - TradutionDb.ids.size() == 0) {
            loredutch.add(ChatColor.GRAY + "Volledig vertaald!");
        }
        loredutch.add(" ");
        loredutch.add(ChatColor.YELLOW + "Klik om uw taal te wijzigen");
        metadutch.setLore(loredutch);
        dutch.setItemMeta(metadutch);

        ItemStack korean = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "6053c959-5f6c-4000-b468-a75230f0fb9a", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZmMxYmU1ZjEyZjQ1ZTQxM2VkYTU2ZjNkZTk0ZTA4ZDkwZWRlOGUzMzljN2IxZThmMzI3OTczOTBlOWE1ZiJ9fX0=");
        SkullMeta metakorean = (SkullMeta) korean.getItemMeta();
        metakorean.setDisplayName(ChatColor.GREEN + "한국");
        ArrayList<String> lorekorean = new ArrayList<String>();
        lorekorean.add(ChatColor.GRAY + "언어를 한국어로 변경하십시오.");
        lorekorean.add(" ");
        lorekorean.add(ChatColor.GRAY + "번역 된 퍼센트");
        lorekorean.add(percentMsg((double) TradutionDb.korean.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.korean.size() - TradutionDb.ids.size() == 0) {
            lorekorean.add(ChatColor.GRAY + "완전 번역!");
        }
        lorekorean.add(" ");
        lorekorean.add(ChatColor.YELLOW + "언어를 변경하려면 클릭");
        metakorean.setLore(lorekorean);
        korean.setItemMeta(metakorean);

        ItemStack chinese = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "2ee56f96-3d1e-4d4d-a259-3828989023ce", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvN2Y5YmMwMzVjZGM4MGYxYWI1ZTExOThmMjlmM2FkM2ZkZDJiNDJkOWE2OWFlYjY0ZGU5OTA2ODE4MDBiOThkYyJ9fX0=");
        SkullMeta metachinese = (SkullMeta) chinese.getItemMeta();
        metachinese.setDisplayName(ChatColor.GREEN + "中国人");
        ArrayList<String> lorechinese = new ArrayList<String>();
        lorechinese.add(ChatColor.GRAY + "将您的语言更改为中文。");
        lorechinese.add(" ");
        lorechinese.add(ChatColor.GRAY + "翻译百分比");
        lorechinese.add(percentMsg((double) TradutionDb.chinese.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.chinese.size() - TradutionDb.ids.size() == 0) {
            lorechinese.add(ChatColor.GRAY + "完全翻译！");
        }
        lorechinese.add(" ");
        lorechinese.add(ChatColor.YELLOW + "点击更改您的语言");
        metachinese.setLore(lorechinese);
        chinese.setItemMeta(metachinese);

        ItemStack french = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "395a599f-9588-4fe2-ada9-07cd81262996", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNTEyNjlhMDY3ZWUzN2U2MzYzNWNhMWU3MjNiNjc2ZjEzOWRjMmRiZGRmZjk2YmJmZWY5OWQ4YjM1Yzk5NmJjIn19fQ==");
        SkullMeta metafrench = (SkullMeta) french.getItemMeta();
        metafrench.setDisplayName(ChatColor.GREEN + "Le français");
        ArrayList<String> lorefrench = new ArrayList<String>();
        lorefrench.add(ChatColor.GRAY + "Changez votre langue en français.");
        lorefrench.add(" ");
        lorefrench.add(ChatColor.GRAY + "Pourcentage traduit");
        lorefrench.add(percentMsg((double) TradutionDb.french.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.french.size() - TradutionDb.ids.size() == 0) {
            lorefrench.add(ChatColor.GRAY + "Entièrement traduit!");
        }
        lorefrench.add(" ");
        lorefrench.add(ChatColor.YELLOW + "Cliquez pour changer votre langue");
        metafrench.setLore(lorefrench);
        french.setItemMeta(metafrench);

        ItemStack russian = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "849c610b-a1fe-48eb-840b-3fe775f6af6f", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMTZlYWZlZjk4MGQ2MTE3ZGFiZTg5ODJhYzRiNDUwOTg4N2UyYzQ2MjFmNmE4ZmU1YzliNzM1YTgzZDc3NWFkIn19fQ==");
        SkullMeta metarussian = (SkullMeta) russian.getItemMeta();
        metarussian.setDisplayName(ChatColor.GREEN + "русский");
        ArrayList<String> lorerussian = new ArrayList<String>();
        lorerussian.add(ChatColor.GRAY + "Измените свой язык на русский.");
        lorerussian.add(" ");
        lorerussian.add(ChatColor.GRAY + "Процент переведен");
        lorerussian.add(percentMsg((double) TradutionDb.russian.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.russian.size() - TradutionDb.ids.size() == 0) {
            lorerussian.add(ChatColor.GRAY + "Полностью переведено!");
        }
        lorerussian.add(" ");
        lorerussian.add(ChatColor.YELLOW + "Нажмите, чтобы изменить свой язык");
        metarussian.setLore(lorerussian);
        russian.setItemMeta(metarussian);

        ItemStack italian = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "a17baafd-e77e-4884-8c47-e7a08146147e", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODVjZTg5MjIzZmE0MmZlMDZhZDY1ZDhkNDRjYTQxMmFlODk5YzgzMTMwOWQ2ODkyNGRmZTBkMTQyZmRiZWVhNCJ9fX0=");
        SkullMeta metaitalian = (SkullMeta) italian.getItemMeta();
        metaitalian.setDisplayName(ChatColor.GREEN + "Italiano");
        ArrayList<String> loreitalian = new ArrayList<String>();
        loreitalian.add(ChatColor.GRAY + "Cambia la tua lingua in italiano.");
        loreitalian.add(" ");
        loreitalian.add(ChatColor.GRAY + "Percentuale tradotta");
        loreitalian.add(percentMsg((double) TradutionDb.italian.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.italian.size() - TradutionDb.ids.size() == 0) {
            loreitalian.add(ChatColor.GRAY + "Completamente tradotto!");
        }
        loreitalian.add(" ");
        loreitalian.add(ChatColor.YELLOW + "Fai clic per cambiare la lingua");
        metaitalian.setLore(loreitalian);
        italian.setItemMeta(metaitalian);

        ItemStack german = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "be211c23-d8aa-4119-bd0d-7f50fd115d9f", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWU3ODk5YjQ4MDY4NTg2OTdlMjgzZjA4NGQ5MTczZmU0ODc4ODY0NTM3NzQ2MjZiMjRiZDhjZmVjYzc3YjNmIn19fQ==");
        SkullMeta metagerman = (SkullMeta) german.getItemMeta();
        metagerman.setDisplayName(ChatColor.GREEN + "Deutsch");
        ArrayList<String> loregerman = new ArrayList<String>();
        loregerman.add(ChatColor.GRAY + "Ändern Sie Ihre Sprache in Deutsch.");
        loregerman.add(" ");
        loregerman.add(ChatColor.GRAY + "Prozent übersetzt");
        loregerman.add(percentMsg((double) TradutionDb.german.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.german.size() - TradutionDb.ids.size() == 0) {
            loregerman.add(ChatColor.GRAY + "Vollständig übersetzt!");
        }
        loregerman.add(" ");
        loregerman.add(ChatColor.YELLOW + "Klicken Sie hier, um Ihre Sprache zu ändern");
        metagerman.setLore(loregerman);
        german.setItemMeta(metagerman);

        ItemStack spanish = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "884a57c8-27ad-4b50-b42b-bee01239f4a8", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvMzJiZDQ1MjE5ODMzMDllMGFkNzZjMWVlMjk4NzQyODc5NTdlYzNkOTZmOGQ4ODkzMjRkYThjODg3ZTQ4NWVhOCJ9fX0=");
        SkullMeta metaspanish = (SkullMeta) spanish.getItemMeta();
        metaspanish.setDisplayName(ChatColor.GREEN + "Espanol");
        ArrayList<String> lorespanish = new ArrayList<String>();
        lorespanish.add(ChatColor.GRAY + "Cambia tu idioma al español.");
        lorespanish.add(" ");
        lorespanish.add(ChatColor.GRAY + "Porcentaje traducido");
        lorespanish.add(percentMsg((double) TradutionDb.spanish.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.spanish.size() - TradutionDb.ids.size() == 0) {
            lorespanish.add(ChatColor.GRAY + "Totalmente traducido!");
        }
        lorespanish.add(" ");
        lorespanish.add(ChatColor.YELLOW + "Haz clic para cambiar tu idioma");
        metaspanish.setLore(lorespanish);
        spanish.setItemMeta(metaspanish);

        ItemStack portuguese = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "d7d1ff81-c959-4670-a0ea-a6220ee15640", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvOWE0NjQ3NWQ1ZGNjODE1ZjZjNWYyODU5ZWRiYjEwNjExZjNlODYxYzBlYjE0ZjA4ODE2MWIzYzBjY2IyYjBkOSJ9fX0=");
        SkullMeta metaportuguese = (SkullMeta) portuguese.getItemMeta();
        metaportuguese.setDisplayName(ChatColor.GREEN + "Português");
        ArrayList<String> loreportuguese = new ArrayList<String>();
        loreportuguese.add(ChatColor.GRAY + "Mude o seu idioma para Português.");
        loreportuguese.add(" ");
        loreportuguese.add(ChatColor.GRAY + "Porcentagem traduzida");
        loreportuguese.add(percentMsg((double) TradutionDb.portuguese.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.portuguese.size() - TradutionDb.ids.size() == 0) {
            loreportuguese.add(ChatColor.GRAY + "Totalmente traduzido!");
        }
        loreportuguese.add(" ");
        loreportuguese.add(ChatColor.YELLOW + "Clique para mudar seu idioma");
        metaportuguese.setLore(loreportuguese);
        portuguese.setItemMeta(metaportuguese);

        ItemStack english = setSkullOwner(new ItemStack(Material.LEGACY_SKULL_ITEM, 1, (short) 3), "3c30484a-76d3-4cfe-88e5-e7599bc9ac4d", "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNGNhYzk3NzRkYTEyMTcyNDg1MzJjZTE0N2Y3ODMxZjY3YTEyZmRjY2ExY2YwY2I0YjM4NDhkZTZiYzk0YjQifX19");
        SkullMeta metaenglish = (SkullMeta) english.getItemMeta();
        metaenglish.setDisplayName(ChatColor.GREEN + "English");
        ArrayList<String> loreenglish = new ArrayList<String>();
        loreenglish.add(ChatColor.GRAY + "Change your language to English.");
        loreenglish.add(" ");
        loreenglish.add(ChatColor.GRAY + "Percent translated");
        loreenglish.add(percentMsg((double) TradutionDb.english.size() / (double) TradutionDb.ids.size() * 100.0));
        if (TradutionDb.english.size() - TradutionDb.ids.size() == 0) {
            loreenglish.add(ChatColor.GRAY + "Fully translated!");
        }
        loreenglish.add(" ");
        loreenglish.add(ChatColor.YELLOW + "Click to change your language");
        metaenglish.setLore(loreenglish);
        english.setItemMeta(metaenglish);

        inventory.setItem(10, english);
        inventory.setItem(11, portuguese);
        inventory.setItem(12, spanish);
        inventory.setItem(13, german);
        inventory.setItem(14, italian);
        inventory.setItem(15, russian);
        inventory.setItem(16, french);
        inventory.setItem(20, chinese);
        inventory.setItem(21, korean);
        inventory.setItem(22, dutch);
        inventory.setItem(23, polish);
        inventory.setItem(24, japanese);

        return inventory;
    }

    private static String percentMsg(double percent) {
        int toint = (int) percent;
        String msg = "";
        int added = 20;
        if (toint <= 30) {
            while (toint > 0) {
                if (toint >= 5) {
                    toint -= 5;
                    added--;
                    msg += ChatColor.RED + "⬛";
                } else toint = 0;
            }

            while (added > 0) {
                added--;
                msg += ChatColor.DARK_GRAY + "⬛";
            }
        } else if (toint >= 31 && toint <= 69) {
            while (toint > 0) {
                if (toint >= 5) {
                    toint -= 5;
                    added--;
                    msg += ChatColor.GOLD + "⬛";
                } else toint = 0;
            }

            while (added > 0) {
                added--;
                msg += ChatColor.DARK_GRAY + "⬛";
            }
        } else if (toint >= 70) {
            while (toint > 0) {
                if (toint >= 5) {
                    toint -= 5;
                    added--;
                    msg += ChatColor.GREEN + "⬛";
                } else toint = 0;
            }

            while (added > 0) {
                added--;
                msg += ChatColor.DARK_GRAY + "⬛";
            }
        }

        return msg;
    }

    public static ItemStack setSkullOwner(final ItemStack itemStack, final String id, final String textureValue) {
        final net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(itemStack);

        NBTTagCompound compound = nmsStack.getTag();
        if (compound == null) {
            compound = new NBTTagCompound();
            nmsStack.setTag(compound);
            compound = nmsStack.getTag();
        }

        final NBTTagCompound skullOwner = new NBTTagCompound();
        skullOwner.set("Id", NBTTagString.a(textureValue));
        final NBTTagCompound properties = new NBTTagCompound();
        final NBTTagList textures = new NBTTagList();
        final NBTTagCompound value = new NBTTagCompound();
        value.set("Value", NBTTagString.a(textureValue));
        textures.add(value);
        properties.set("textures", textures);
        skullOwner.set("Properties", properties);

        compound.set("SkullOwner", skullOwner);
        nmsStack.setTag(compound);

        return CraftItemStack.asBukkitCopy(nmsStack);
    }
}
