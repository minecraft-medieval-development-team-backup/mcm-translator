package com.mcm.translator;

import com.mcm.translator.listeners.InventoryLanguages;
import com.mcm.translator.listeners.PlayerJoinEvents;
import com.mcm.translator.listeners.PlayerPickupItemCase;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new InventoryLanguages(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerPickupItemCase(), Main.plugin);
    }
}
